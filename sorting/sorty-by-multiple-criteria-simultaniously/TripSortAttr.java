
/**
 * TripSortAttr is trip attributes container used for sorting trips. {@link TripSortAttr#price},
 * {@link TripSortAttr#duration} and {@link TripSortAttr#arrivalHour} are final values.
 *
 * <p>{@link TripSortAttr#centralRatio} and {@link TripSortAttr#surfaceIntegral} are calculated by
 * {@link TripSortAttr#calculateSortAttr(float)}
 */
@AllArgsConstructor
public class TripSortAttr {
  private static final int PREFER_ARRIVAL_HOUR_FROM = 6;
  private static final int PREFER_ARRIVAL_HOUR_TO = 12;
  private static final float PREFER_ARRIVAL_HOUR_FACTOR = 2F;

  private final long price;
  private final long duration;
  // hour from 0 to 23 starting from midnight of arrival local time
  private final int arrivalHour;
  // proximity to median relation between price and duration
  public float centralRatio;
  // multiplication of price by duration with arriving hour fixation
  public double surfaceIntegral;

  public void calculateSortAttr(float averagePriceToDurationRatio) {
    centralRatio = Math.abs(averagePriceToDurationRatio - 1f * price / duration);
    surfaceIntegral = Math.sqrt(price) * duration;
    if (PREFER_ARRIVAL_HOUR_FROM <= arrivalHour && arrivalHour <= PREFER_ARRIVAL_HOUR_TO) {
      surfaceIntegral /= PREFER_ARRIVAL_HOUR_FACTOR;
    }
  }
}
