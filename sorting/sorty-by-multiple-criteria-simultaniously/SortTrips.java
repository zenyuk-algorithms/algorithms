
/**
 * SortTrips sorts trips in order using trips {@link TripSortAttr} calculated trip attributes
 *
 * <p>Basic idea is that we calculate average trip Price and average trip Duration. Use the averages
 * as a median to compare with each trip's values stored in {@link TripSortAttr#centralRatio}. Give
 * preference to a trip that is closer to the median. That make odd trips like one with super cheap
 * Price but extraordinary long Duration less preferable.
 *
 * <p>We calculate trip's weight, a multiplication of Price by Duration. Take smaller one as it's
 * cheaper and faster.
 *
 * <p>Give preference to trips arriving before hotel check-in time plus commute to hotel. Usual
 * check-in cut off is 14:00 and giving 2 hours to get from airport to hotel.
 */
@Slf4j
public class SortTrips {
  private static final int SURFACE_INTEGRAL_DIFF_PERCENT = 5;

  // sort the list in preferred order, considering Price, Trip Duration and Arrival Hour.
  public static List<CombinedResult> sortByPriceDurationArrivalHour(List<CombinedResult> input) {
    // Key is flight's original index in the given list. Value - calculated to order trips
    Map<Integer, TripSortAttr> sortTripsAttr = new HashMap<>(input.size());

    long[] totalPriceAndDuration =
        input.stream()
            .map(
                trip -> {
                  final long price = trip.getCheapestCompleteTrip().getTotal().intValue();
                  final long duration = totalTripDuration(trip.getJourneys());
                  final int arrivalHour = arrivalHour(trip.getJourneys());
                  TripSortAttr tripSortAttr = new TripSortAttr(price, duration, arrivalHour, 0, 0);
                  sortTripsAttr.put(trip.hashCode(), tripSortAttr);
                  return new long[] {price, duration};
                })
            .reduce(SortTrips::reduceTuplesOfPriceAndDuration)
            .orElse(new long[] {1, 1});

    final float averagePriceToDurationRatio =
        averagePriceToDurationRatio(input.size(), totalPriceAndDuration);

    sortTripsAttr.values().forEach(t -> t.calculateSortAttr(averagePriceToDurationRatio));
    input.sort((o1, o2) -> SortTrips.compareSurfaceIntegralAndCentralRatio(o1, o2, sortTripsAttr));
    return input;
  }

  // sort the list in preferred order, considering Price, Trip Duration and Arrival Hour.
  public static List<FareDetailResult> sortByPriceDurationArrivalHourFareDetail(
      List<FareDetailResult> input) {
    // Key is flight's original index in the given list. Value - calculated to order trips
    Map<Integer, TripSortAttr> sortTripsAttr = new HashMap<>(input.size());

    long[] totalPriceAndDuration =
        input.stream()
            .map(
                trip -> {
                  final long price = trip.getPrice().getAmount().intValue();
                  final long duration = totalTripDurationFamilyInfo(trip.getJourneyFamilies());
                  final int arrivalHour = arrivalHourFamilyInfo(trip.getJourneyFamilies());
                  TripSortAttr tripSortAttr = new TripSortAttr(price, duration, arrivalHour, 0, 0);
                  sortTripsAttr.put(trip.hashCode(), tripSortAttr);
                  return new long[] {price, duration};
                })
            .reduce(SortTrips::reduceTuplesOfPriceAndDuration)
            .orElse(new long[] {1, 1});

    final float averagePriceToDurationRatio =
        averagePriceToDurationRatio(input.size(), totalPriceAndDuration);

    sortTripsAttr.values().forEach(t -> t.calculateSortAttr(averagePriceToDurationRatio));
    input.sort((o1, o2) -> SortTrips.compareSurfaceIntegralAndCentralRatio(o1, o2, sortTripsAttr));
    return input;
  }

  // sort trips by Price, Trip Duration and Arrival Hour
  static int compareSurfaceIntegralAndCentralRatio(
      Object o1, Object o2, Map<Integer, TripSortAttr> sortingAttributes) {
    TripSortAttr attr1 = sortingAttributes.get(o1.hashCode());
    TripSortAttr attr2 = sortingAttributes.get(o2.hashCode());
    if (attr1 == null || attr2 == null) {
      return 0;
    }
    // if price by duration difference is within 5% take more balanced one (closest to their median)
    double diff = Math.abs(attr1.surfaceIntegral - attr2.surfaceIntegral);
    // treat trips within particular percent difference as the same
    if (attr1.surfaceIntegral / 100 * SURFACE_INTEGRAL_DIFF_PERCENT > diff) {
      if (attr1.centralRatio > attr2.centralRatio) {
        return 1;
      } else if (attr1.centralRatio < attr2.centralRatio) {
        return -1;
      }
      return 0;
    } else if (attr1.surfaceIntegral > attr2.surfaceIntegral) {
      return 1;
    } else {
      return -1;
    }
  }

  static int totalTripDuration(List<JourneyFlightInfo> flightInfos) {
    return flightInfos.stream().map(SortTrips::getFlightDuration).reduce(0, Integer::sum);
  }

  static int totalTripDurationFamilyInfo(List<JourneyFlightFamilyInfo> flightInfos) {
    return flightInfos.stream()
        .map(jffi -> jffi.getJourney())
        .map(SortTrips::getFlightDuration)
        .reduce(0, Integer::sum);
  }

  private static Integer getFlightDuration(JourneyFlightInfo fi) {
    if (fi.getLayovers() == null) {
      return fi.getDuration();
    }
    return fi.getLayovers().stream()
        .map(Layover::getDuration)
        .reduce(fi.getDuration(), Integer::sum);
  }

  static int arrivalHour(List<JourneyFlightInfo> input) {
    List<FlightLeg> legs = input.get(input.size() - 1).getLegs();
    return legs.get(legs.size() - 1).getArrivalTime().getHour();
  }

  static int arrivalHourFamilyInfo(List<JourneyFlightFamilyInfo> input) {

    JourneyFlightInfo journey = input.get(input.size() - 1).getJourney();
    if (journey == null || CollectionUtils.isEmpty(journey.getLegs())) {
      throw new PackageException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          ErrorCode.INTERNAL_ERROR,
          "JourneyFlightFamilyInfo" + " does NOT contain JourneyFlightInfo");
    }
    return journey.getLegs().get(journey.getLegs().size() - 1).getArrivalTime().getHour();
  }

  static long[] reduceTuplesOfPriceAndDuration(long[] priceAndDuration1, long[] priceAndDuration2) {
    long price = priceAndDuration1[0] + priceAndDuration2[0];
    long duration = priceAndDuration1[1] + priceAndDuration2[1];
    return new long[] {price, duration};
  }

  static float averagePriceToDurationRatio(int allTripsCount, long[] totalPriceAndDurationTuple) {
    if (allTripsCount == 0) {
      log.warn("averagePriceToDurationRatio, number of trips is 0");
      return 1;
    }
    final long averagePrice = totalPriceAndDurationTuple[0] / allTripsCount;
    final long averageDuration = totalPriceAndDurationTuple[1] / allTripsCount;
    if (averageDuration == 0) {
      log.warn("averagePriceToDurationRatio, average trip duration is 0");
      return 1;
    }
    return 1f * averagePrice / averageDuration;
  }
}
